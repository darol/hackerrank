package algorithms;

import org.junit.Assert;
import org.junit.Test;

public class SurfaceAreaTest {

    @Test
    public void testValidArea(){
        Assert.assertEquals(60, SurfaceArea.surfaceArea(new int[][] {{1,3,4}, {2,2,3}, {1,2,4}}));
    }
}