package algorithms.easy;

import org.junit.Assert;
import org.junit.Test;

public class EqualizeTheArrayTest {
    @Test
    public void equalizeArray() {
        Assert.assertEquals(2, EqualizeTheArray.equalizeArray(new int[]{3, 3, 2, 1, 3}));
        Assert.assertEquals(23, EqualizeTheArray.equalizeArray(new int[]{10, 27, 9, 10, 100, 38, 30, 32, 45, 29, 27, 29, 32, 38, 32, 38, 14, 38, 29, 30, 63, 29, 63, 91, 54, 10, 63}));
    }
}