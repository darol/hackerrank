package algorithms.easy;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class JimAndTheOrdersTest {

    @Test
    public void jimOrders() {
        Assert.assertArrayEquals(new int[]{4, 2, 5, 1, 3}, JimAndTheOrders.jimOrders(new int[][]{{8, 1}, {4, 2}, {5, 6}, {3, 1}, {4, 3}}));
        Assert.assertArrayEquals(new int[]{1, 2}, JimAndTheOrders.jimOrders(new int[][]{{1, 1}, {1, 1}}));
    }
}