package algorithms.easy;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class GridChallengeTest {

    @Test
    public void gridChallenge() {
        Assert.assertEquals("YES", GridChallenge.gridChallenge(new String[]{"ebacd", "fghij", "olmkn", "trpqs", "xywuv"}));
    }
}