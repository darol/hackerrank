package algorithms.easy;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class TimeConversionTest {

    @Test
    public void timeConversion() {
        Assert.assertEquals("19:05:45", TimeConversion.timeConversion("07:05:45PM"));
        Assert.assertEquals("12:00:00", TimeConversion.timeConversion("12:00:00PM"));
        Assert.assertEquals("23:00:00", TimeConversion.timeConversion("11:00:00PM"));
        Assert.assertEquals("00:00:00", TimeConversion.timeConversion("12:00:00AM"));
        Assert.assertEquals("01:00:00", TimeConversion.timeConversion("01:00:00AM"));
        Assert.assertEquals("11:00:00", TimeConversion.timeConversion("11:00:00AM"));
    }
}