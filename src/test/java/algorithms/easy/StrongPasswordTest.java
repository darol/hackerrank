package algorithms.easy;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class StrongPasswordTest {

    @Test
    public void minimumNumber() {
        Assert.assertEquals(1, StrongPassword.minimumNumber(11, "#HackerRank"));
        Assert.assertEquals(3, StrongPassword.minimumNumber(3, "Ab1"));
    }
}