package algorithms.easy;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class AnagramTest {

    @Test
    public void anagram() {
        Assert.assertEquals(3, Anagram.anagram("aaabbb"));
        Assert.assertEquals(3, Anagram.anagram("asdfjoieufoa"));
        Assert.assertEquals(5, Anagram.anagram("fdhlvosfpafhalll"));
        Assert.assertEquals(5, Anagram.anagram("mvdalvkiopaufl"));
    }
}