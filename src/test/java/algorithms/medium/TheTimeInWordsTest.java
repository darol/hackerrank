package algorithms.medium;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class TheTimeInWordsTest {

    @Test
    public void test(){
        Assert.assertEquals("thirteen minutes to six", TheTimeInWords.timeInWords(5, 47));
        Assert.assertEquals("three o' clock", TheTimeInWords.timeInWords(3, 00));
        Assert.assertEquals("quarter past seven", TheTimeInWords.timeInWords(7, 15));
        Assert.assertEquals("quarter to six", TheTimeInWords.timeInWords(5, 45));
    }

}