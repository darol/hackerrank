package algorithms.medium;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class ConnectedCellsInAGridTest {

    @Test
    public void connectedCell() {
        Assert.assertEquals(5, ConnectedCellsInAGrid.connectedCell(new int[][]{{1, 1, 0, 0}, {0, 1, 1, 0}, {0, 0, 1, 0}, {1, 0, 0, 0}}));
    }
}