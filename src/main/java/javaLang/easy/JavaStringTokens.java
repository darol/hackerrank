package javaLang.easy;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class JavaStringTokens {

    static void print(String s){
        List<String> split = Arrays.stream(s.split(" |!|,|\\?|\\.|'|_|@"))
                .filter(s1 -> !s1.isEmpty())
                .collect(Collectors.toList());

        System.out.println(split.size());
        split.forEach(System.out::println);
    }

    public static void main(String[] args) {

        print("He is!a,very?very.good_boy'isn@t he?");
    }
}
