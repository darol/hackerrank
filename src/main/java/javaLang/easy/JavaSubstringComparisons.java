package javaLang.easy;

public class JavaSubstringComparisons {
    public static String getSmallestAndLargest(String s, int k) {
        String smallest = "";
        String largest = "";

        if (k < s.length()) {
            String[] subStrings = new String[s.length() - k + 1];

            for (int i = 0; i < s.length() - k + 1; i++) {
                subStrings[i] = s.substring(i, i + k);
            }

            smallest = subStrings[0];
            largest = subStrings[0];

            for (int i = 1; i < subStrings.length ; i++) {
                if(compare(smallest, subStrings[i]) == 1) smallest = subStrings[i];
                if(compare(largest, subStrings[i]) == -1) largest = subStrings[i];
            }
        } else if (k == s.length()) {
            smallest = s;
            largest = s;
        }

        return smallest + "\n" + largest;
    }

    private static int compare(String o1, String o2) {
        for (int i = 0; i < o1.length(); i++) {
            if(o1.charAt(i) != o2.charAt(i)){
                return Integer.compare(o1.charAt(i), o2.charAt(i));
            }
        }
        return 0;

//        String.valueOf()
    }

    public static void main(String[] args) {
        System.out.println(getSmallestAndLargest("welcometojava", 3));
    }
}
