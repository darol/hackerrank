package algorithms.easy;

import java.util.TreeMap;

public class EqualizeTheArray {
    static int equalizeArray(int[] arr) {
        TreeMap<Integer, Integer> map = new TreeMap<>();

        for (int i = 0; i < arr.length; i++) {
            Integer integer = map.get(arr[i]);
            map.put(arr[i], integer == null ? 1 : integer + 1);
        }

        return arr.length - map.values().stream().mapToInt(value -> value).max().orElse(0);
    }
}
