package algorithms.easy;

import java.util.Arrays;
import java.util.stream.IntStream;

public class Anagram {
    static int anagram(String s) {
        if (s.isEmpty() || s.length() % 2 != 0) return -1;

        char[] left = s.substring(0, s.length() / 2).toCharArray();
        char[] right = s.substring(s.length() / 2).toCharArray();

        Arrays.sort(left);
        Arrays.sort(right);

        return (int) IntStream.range(0, left.length).filter(i -> left[i] != right[i]).count();
    }
}
