package algorithms.easy;

import java.util.Arrays;

public class GridChallenge {
    static String gridChallenge(String[] grid) {
        grid = sort(grid);
        return isValid(grid) ? "YES" : "NO";
    }

    private static String[] sort(String[] grid) {
        for (int i = 0; i < grid.length; i++) {
            char[] charArray = grid[i].toCharArray();
            Arrays.sort(charArray);
            grid[i] = new String(charArray);
        }

        return grid;
    }

    private static boolean isValid(String[] grid) {
        if (grid.length > 0 && grid[0].length() > 0) {
            for (int i = 0; i < grid[0].length(); i++) {
                for (int j = 0; j < grid.length - 1; j++) {
                    if (grid[j].charAt(i) > grid[j + 1].charAt(i)) {
                        return false;
                    }
                }
            }
        }

        return true;
    }
}
