package algorithms.easy;

import java.util.stream.IntStream;

public class Staircase {
    static void staircase(int n) {
        for (int i = 1; i <= n; i++) {
            IntStream.range(0, n - i).forEach(value -> System.out.print(" "));
            IntStream.range(0, i).forEach(value -> System.out.print("#"));
            if(i < n) System.out.println();
        }
    }

    public static void main(String[] args) {
        staircase(20);
    }
}
