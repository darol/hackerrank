package algorithms.easy;

public class StrongPassword {

    static String numbers = "0123456789";
    static String lower_case = "abcdefghijklmnopqrstuvwxyz";
    static String upper_case = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    static String special_characters = "!@#$%^&*()-+";

    static int minimumNumber(int n, String password) {
        int strength = getStrength(password, numbers) +
                getStrength(password, lower_case) +
                getStrength(password, upper_case) +
                getStrength(password, special_characters);

        strength = 4 - strength;
        int missingChars = 6 - n;

        return n >= 6 ? strength : strength > missingChars ? strength : missingChars;
    }

    static int getStrength(String password, String strongValues){
        return strongValues.chars().mapToObj(value -> Character.toString((char) value)).filter(password::contains).findFirst().map(s -> 1).orElse(0);
    }
}
