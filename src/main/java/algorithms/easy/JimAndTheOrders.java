package algorithms.easy;

import java.util.*;

public class JimAndTheOrders {

    static int[] jimOrders(int[][] orders) {
        List<ClientOrder> clientOrderList = new ArrayList<>();

        for (int i = 0; i < orders.length; i++) {
            clientOrderList.add(new ClientOrder(i + 1, orders[i][0] + orders[i][1]));
        }

        clientOrderList.sort(Comparator.comparingInt(o -> o.time));

        return clientOrderList.stream().mapToInt(value -> value.client).toArray();
    }

    private static class ClientOrder{
        private final int client;
        private final int time;

        public ClientOrder(int client, int time) {
            this.client = client;
            this.time = time;
        }
    }
}

