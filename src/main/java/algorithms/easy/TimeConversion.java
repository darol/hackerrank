package algorithms.easy;

public class TimeConversion {

    static String timeConversion(String s) {
        String[] split = s.split(":");
        StringBuilder stringBuilder = new StringBuilder();
        int hour = Integer.parseInt(split[0]);

        if (s.contains("PM")) {
            hour = hour == 12 ? 12 : hour + 12;
        } else {
            hour = hour == 12 ? 0 : hour;
        }

        return stringBuilder.append(hour < 10 ? "0" + hour : hour).append(":")
                .append(split[1]).append(":")
                .append(split[2], 0, 2).toString();
    }
}
